/* set.h
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SET_H
#define SET_H

#include <glib.h>
#include <string.h>

G_BEGIN_DECLS

#define SET_DEFINE(TypeName, type_name, element_type, compare) \
typedef struct { \
   guint len; \
   element_type *elements; \
} TypeName; \
\
void type_name##_init (TypeName *set) \
{ \
   memset (set, 0, sizeof *set); \
} \
\
static gboolean \
type_name##_bsearch (const TypeName *set, \
                     element_type    target, \
                     gint           *pos) \
{ \
   gint left; \
   gint middle = 0; \
   gint right; \
   gint cmpval; \
   \
   *pos = 0; \
   \
   if (!set->len) { \
      return FALSE; \
   } \
   \
   left = 0; \
   right = set->len - 1; \
   \
   while (left <= right) { \
      middle = (left + right) / 2; \
      cmpval = compare (&set->elements [middle], &target); \
      if (cmpval < 0) { \
         left = middle + 1; \
      } else if (cmpval > 0) { \
         right = middle - 1; \
      } else { \
         *pos = middle; \
         return TRUE; \
      } \
   } \
   \
   *pos = middle; \
   \
   return FALSE; \
} \
\
gboolean \
type_name##_contains (const TypeName *set, \
                      element_type value) \
{ \
   gint pos; \
   g_return_val_if_fail (set, FALSE); \
   return type_name##_bsearch (set, value, &pos); \
} \
void \
type_name##_insert (TypeName *set, \
                    element_type value) \
{ \
   gsize new_size; \
   gint pos = 0; \
   \
   g_return_if_fail (set); \
   g_return_if_fail (set->len < G_MAXINT); \
   \
   if (type_name##_bsearch (set, value, &pos)) { \
      set->elements [pos] = value; \
      return; \
   } \
   \
   new_size = sizeof value * (set->len + 1); \
   set->elements = g_realloc (set->elements, new_size); \
   \
   if (!set->len || (pos == set->len)) { \
      set->elements [set->len++] = value; \
      return; \
   } \
   \
   if (compare (&value, &set->elements [pos]) > 0) { \
      pos++; \
   } \
   \
   memmove (&set->elements [pos + 1], &set->elements [pos], \
            sizeof value * (set->len - pos)); \
   set->elements [pos] = value; \
   set->len++; \
} \
void \
type_name##_remove (TypeName *set, \
                    element_type value) \
{ \
   gint pos; \
   g_return_if_fail (set); \
   if (type_name##_bsearch (set, value, &pos)) { \
      memmove (&set->elements [pos], &set->elements [pos+1], \
               sizeof value * (set->len - pos)); \
      set->elements = g_realloc (set->elements, \
                                 sizeof value * (set->len - 1)); \
   } \
} \
\
void \
type_name##_destroy (TypeName *set) \
{ \
   g_free (set->elements); \
} \
\
void \
type_name##_intersect (const TypeName *set1, \
                       const TypeName *set2, \
                       TypeName *dest) \
{ \
   int cmp; \
   int i; \
   int j = 0; \
   \
   type_name##_init (dest); \
   \
   for (i = 0; i < set1->len; i++) { \
      for (; j < set2->len; j++) { \
         cmp = compare (&set1->elements [i], &set2->elements [j]); \
         if (cmp == 0) { \
            type_name##_insert (dest, set1->elements [i]); \
            j++; \
            break; \
         } else if (cmp > 0) { \
            break; \
         } \
      } \
   } \
} \
\
void \
type_name##_difference (const TypeName *set1, \
                        const TypeName *set2, \
                        TypeName *dest) \
{ \
   int i; \
   \
   type_name##_init (dest); \
   \
   for (i = 0; i < set1->len; i++) { \
      if (!type_name##_contains (set2, set1->elements [i])) { \
         type_name##_insert (dest, set1->elements [i]); \
      } \
   } \
   \
   for (i = 0; i < set2->len; i++) { \
      if (!type_name##_contains (set1, set2->elements [i])) { \
         type_name##_insert (dest, set2->elements [i]); \
      } \
   } \
} \
\
void \
type_name##_union (const TypeName *set1, \
                   const TypeName *set2, \
                   TypeName *dest) \
{ \
   int i; \
   int j; \
   \
   type_name##_init (dest); \
   \
   for (i = 0, j = 0; i < set1->len; i++) { \
      for (; compare (&set2->elements [j], &set1->elements [i]) < 0; j++) { \
         type_name##_insert (dest, set2->elements [j]); \
      } \
      type_name##_insert (dest, set1->elements [i]); \
   } \
   \
   for (; j < set2->len; j++) { \
      type_name##_insert (dest, set2->elements [j]); \
   } \
} \
\
gboolean \
type_name##_is_subset (const TypeName *set1, \
                       const TypeName *set2) \
{ \
   int i; \
   \
   for (i = 0; i < set2->len; i++) { \
      if (!type_name##_contains (set1, set2->elements [i])) { \
         return FALSE; \
      } \
   } \
   \
   return TRUE; \
} \
\
gboolean \
type_name##_equal (const TypeName *set1, \
                   const TypeName *set2) \
{ \
   int i; \
   \
   if (set1->len == set2->len) { \
      for (i = 0; i < set1->len; i++) { \
         if (!type_name##_contains (set2, set1->elements [i])) { \
            return FALSE; \
         } \
      } \
      return TRUE; \
   } \
   \
   return FALSE; \
}

G_END_DECLS

#endif /* SET_H */
