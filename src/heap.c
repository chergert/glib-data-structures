/* heap.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "heap.h"

/*
 * The following implementation is based upon Chapter 10 of "Mastering
 * Algorithms in C" by Kyle Loudon. ISBN 1-56592-453-3.
 */

struct _Heap
{
   gint size;
   GCompareFunc compare;
   GDestroyNotify destroy;
   gpointer *tree;
};

#define heap_parent(npos)   (((npos)-1)/2)
#define heap_left(npos)     (((npos)*2)+1)
#define heap_right(npos)    (((npos)*2)+2)
#define heap_swap(heap,a,b) \
   G_STMT_START { \
      gpointer temp = heap->tree [a]; \
      heap->tree [a] = heap->tree [b]; \
      heap->tree [b] = temp; \
   } G_STMT_END

Heap *
heap_new (GCompareFunc   compare,
          GDestroyNotify destroy)
{
   Heap *heap;

   heap = g_new0 (Heap, 1);
   heap->size = 0;
   heap->compare = compare;
   heap->destroy = destroy;
   heap->tree = NULL;

   return heap;
}

void
heap_free (Heap *heap)
{
   gint i;

   g_return_if_fail (heap);

   if (heap->destroy) {
      for (i = 0; i < heap->size; i++) {
         heap->destroy (heap->tree [i]);
      }
   }

   g_free (heap->tree);
   g_free (heap);
}

void
heap_push (Heap          *heap,
           gconstpointer  value)
{
   gsize new_size;
   gint ipos;
   gint ppos;

   g_return_if_fail (heap);
   g_return_if_fail (heap->size < INT_MAX);

   new_size = sizeof (gpointer) * (heap->size + 1);
   heap->tree = g_realloc (heap->tree, new_size);
   heap->tree [heap->size] = (gpointer)value;

   ipos = heap->size;
   ppos = heap_parent (ipos);

   while ((ipos > 0) &&
          (heap->compare (heap->tree [ppos], heap->tree [ipos]) < 0)) {
      heap_swap (heap, ppos, ipos);

      ipos = ppos;
      ppos = heap_parent (ipos);
   }

   heap->size++;
}

gconstpointer
heap_pop (Heap *heap)
{
   gpointer save;
   gpointer ret = NULL;
   gsize new_size;
   gint ipos;
   gint lpos;
   gint rpos;
   gint mpos;

   g_return_val_if_fail (heap, NULL);

   if (heap->size) {
      ret = heap->tree [0];
      save = heap->tree [heap->size - 1];

      new_size = sizeof (gpointer) * (heap->size - 1);
      heap->tree = g_realloc (heap->tree, new_size);
      heap->size--;

      if (!heap->size) {
         return ret;
      }

      heap->tree [0] = save;
      ipos = 0;

      for (;;) {
         lpos = heap_left (ipos);
         rpos = heap_right (ipos);

         if ((lpos < heap->size) &&
             (heap->compare (heap->tree [lpos], heap->tree [ipos]) > 0)) {
            mpos = lpos;
         } else {
            mpos = ipos;
         }

         if ((rpos < heap->size) &&
             (heap->compare (heap->tree [rpos], heap->tree [mpos]) > 0)) {
            mpos = rpos;
         }

         if (mpos == ipos) {
            break;
         }

         heap_swap (heap, mpos, ipos);

         ipos = mpos;
      }
   }

   return ret;
}

guint
heap_size (Heap *heap)
{
   g_return_val_if_fail (heap, 0);

   return heap->size;
}

gconstpointer
heap_peek_nth (Heap  *heap,
               guint  nth)
{
   g_return_val_if_fail (heap, NULL);

   if (nth < heap->size) {
      return heap->tree [nth];
   }

   return NULL;
}
