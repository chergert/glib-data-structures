/* test-set.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>

#include "set.h"

static int cmpint (const int *a, const int *b)
{
   return *a - *b;
}

static int cmpptr (gpointer *a, gpointer *b)
{
   return GPOINTER_TO_INT ((*a - *b));
}

SET_DEFINE (IntSet, int_set, int, cmpint)
SET_DEFINE (GPointerSet, gpointer_set, gpointer, cmpptr)

static void
test_int_set (void)
{
   IntSet set;
   int i;

   int_set_init (&set);

   int_set_insert (&set, 2);
   int_set_insert (&set, 3);
   int_set_insert (&set, 1);

   g_assert_cmpint (set.len, ==, 3);
   int_set_insert (&set, 1);
   g_assert_cmpint (set.len, ==, 3);

   for (i = 1000; i > 0; i--) {
      int_set_insert (&set, i);
   }

   g_assert_cmpint (set.len, ==, 1000);

   for (i = 1; i < 1000; i++) {
      g_assert (int_set_contains (&set, i));
   }

   g_assert_cmpint (set.len, ==, 1000);

   for (i = 1; i < 1000; i++) {
      g_assert (int_set_contains (&set, i));
   }

   g_assert_cmpint (set.len, ==, 1000);

   int_set_remove (&set, 123);
   g_assert (!int_set_contains (&set, 123));

   for (i = 1; i < 1000; i++) {
      if (i != 123) {
         g_assert (int_set_contains (&set, i));
      }
   }

   int_set_destroy (&set);
}

static void
test_int_set_intersect (void)
{
   IntSet set1 = { 0 };
   IntSet set2 = { 0 };
   IntSet set3;

   int_set_insert (&set1, 1);
   int_set_insert (&set1, 2);
   int_set_insert (&set1, 3);

   int_set_insert (&set2, 1);
   int_set_insert (&set2, 2);
   int_set_insert (&set2, 4);
   int_set_insert (&set2, 5);

   int_set_intersect (&set1, &set2, &set3);

   g_assert (int_set_contains (&set3, 1));
   g_assert (int_set_contains (&set3, 2));
   g_assert (!int_set_contains (&set3, 3));
   g_assert (!int_set_contains (&set3, 4));
   g_assert (!int_set_contains (&set3, 5));
   g_assert (!int_set_contains (&set3, 6));

   int_set_destroy (&set1);
   int_set_destroy (&set2);
   int_set_destroy (&set3);
}

static void
test_int_set_union (void)
{
   IntSet set1 = { 0 };
   IntSet set2 = { 0 };
   IntSet set3;
#if 0
   int i;
#endif

   int_set_insert (&set1, 1);
   int_set_insert (&set1, 2);
   int_set_insert (&set1, 3);
   int_set_insert (&set1, 5);
   int_set_insert (&set1, 6);

   int_set_insert (&set2, 1);
   int_set_insert (&set2, 2);
   int_set_insert (&set2, 4);
   int_set_insert (&set2, 5);

   int_set_union (&set1, &set2, &set3);

   g_assert (int_set_contains (&set3, 1));
   g_assert (int_set_contains (&set3, 2));
   g_assert (int_set_contains (&set3, 3));
   g_assert (int_set_contains (&set3, 4));
   g_assert (int_set_contains (&set3, 5));
   g_assert (int_set_contains (&set3, 6));
   g_assert (!int_set_contains (&set3, 7));

#if 0
   for (i = 0; i < set3.len; i++) {
      g_print ("%d  ", set3.elements [i]);
   }
   g_print ("\n");
#endif

   int_set_destroy (&set1);
   int_set_destroy (&set2);
   int_set_destroy (&set3);
}

static void
test_int_set_difference (void)
{
   IntSet set1 = { 0 };
   IntSet set2 = { 0 };
   IntSet set3;

   int_set_insert (&set1, 1);
   int_set_insert (&set1, 2);
   int_set_insert (&set1, 3);

   int_set_insert (&set2, 1);
   int_set_insert (&set2, 2);
   int_set_insert (&set2, 4);
   int_set_insert (&set2, 5);

   int_set_difference (&set1, &set2, &set3);

   g_assert (!int_set_contains (&set3, 1));
   g_assert (!int_set_contains (&set3, 2));
   g_assert (int_set_contains (&set3, 3));
   g_assert (int_set_contains (&set3, 4));
   g_assert (int_set_contains (&set3, 5));
   g_assert (!int_set_contains (&set3, 6));

   int_set_destroy (&set1);
   int_set_destroy (&set2);
   int_set_destroy (&set3);
}

static void
test_int_set_is_subset (void)
{
   IntSet set1 = { 0 };
   IntSet set2 = { 0 };
   IntSet set3 = { 0 };

   int_set_insert (&set1, 1);
   int_set_insert (&set1, 2);
   int_set_insert (&set1, 3);

   int_set_insert (&set2, 1);
   int_set_insert (&set2, 2);

   int_set_insert (&set3, 1);
   int_set_insert (&set3, 4);

   g_assert (int_set_is_subset (&set1, &set2));
   g_assert (!int_set_is_subset (&set1, &set3));

   int_set_destroy (&set1);
   int_set_destroy (&set2);
   int_set_destroy (&set3);
}

static void
test_int_set_equal (void)
{
   IntSet set1 = { 0 };
   IntSet set2 = { 0 };
   IntSet set3 = { 0 };

   int_set_insert (&set1, 1);
   int_set_insert (&set1, 2);
   int_set_insert (&set1, 3);

   int_set_insert (&set2, 1);
   int_set_insert (&set2, 2);

   int_set_insert (&set3, 1);
   int_set_insert (&set3, 2);
   int_set_insert (&set3, 3);

   g_assert (!int_set_equal (&set1, &set2));
   g_assert (!int_set_equal (&set2, &set3));
   g_assert (int_set_equal (&set1, &set3));

   int_set_destroy (&set1);
   int_set_destroy (&set2);
   int_set_destroy (&set3);
}

static void
test_gpointer_set (void)
{
   GPointerSet set;

   gpointer_set_init (&set);
   gpointer_set_insert (&set, GINT_TO_POINTER (2));
   gpointer_set_insert (&set, GINT_TO_POINTER (3));
   gpointer_set_insert (&set, GINT_TO_POINTER (1));
   gpointer_set_destroy (&set);
}

gint
main (gint   argc,
      gchar *argv[])
{
   test_int_set ();
   test_int_set_intersect ();
   test_int_set_union ();
   test_int_set_difference ();
   test_int_set_is_subset ();
   test_int_set_equal ();

   test_gpointer_set ();

   return EXIT_SUCCESS;
}
