/* test-pqueue.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>

#include "pqueue.h"

static gint
cmpint (gconstpointer a,
        gconstpointer b)
{
   return (gint)(a - b);
}

gint
main (gint   argc,
      gchar *argv[])
{
   gconstpointer val;
   PQueue *pq;
   gint i;

   pq = pqueue_new (cmpint, NULL);

   for (i = 0; i < 100; i++) {
      pqueue_push (pq, GINT_TO_POINTER (i));
   }

#if 0
   for (i = 0; i < pqueue_size (pq); i++) {
      val = heap_peek_nth (pq, i);
      g_print ("%d  ", GPOINTER_TO_INT (val));
   }
   g_print ("\n");
#endif

   while (pqueue_size (pq)) {
      val = pqueue_pop (pq);
      g_print ("%d\n", GPOINTER_TO_INT (val));
   }

   pqueue_free (pq);

   return EXIT_SUCCESS;
}
