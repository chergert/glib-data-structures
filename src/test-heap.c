/* test-heap.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>

#include "heap.h"

static const gchar *test_strings[] = {
   "gnome", "fairy", "goblin", "demon", "pixie", "sprite", "leprechaun",
   "devil"
};

gint
main (gint   argc,
      gchar *argv[])
{
   const gchar *str;
   Heap *heap;
   gint i;

   heap = heap_new ((GCompareFunc)g_strcmp0, NULL);

   for (i = 0; i < G_N_ELEMENTS (test_strings); i++) {
      heap_push (heap, test_strings [i]);
   }

   while ((str = heap_pop (heap))) {
      g_print ("%s\n", str);
   }

   heap_free (heap);

   return EXIT_SUCCESS;
}
