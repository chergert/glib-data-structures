# Example Data Structures for GLib

This is a repository of example data structures written using GLib. Feel free
to do what you like with them.

 * heap - A basic heap implementation based on Mastering Algorithms in C.
 * pqueue - A basic priority queue implementation basedon Mastering Algorithms in C.
 * set - (not finished) A basic set implementation with insertion sort.

